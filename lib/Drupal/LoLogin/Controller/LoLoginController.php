<?php

  
 
namespace Drupal\LoLogin\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Access\CsrfTokenGenerator;
use Drupal\user\RegisterFormController;


 
class LoLoginController extends ControllerBase{
 
  /*
  * Function for save new users from Facebook
  */  

  public function saveUser($details) {
    
    //fields for database
    $fields = array(
         'name' =>    $details['name'],
         'status' =>  1,
         'created' => time(),
         'pass' =>    $details['id']
       );

    
    //create new user
    $account = \Drupal::entityManager()
            ->getStorageController("user")
            ->create($fields);

    //save created user
    $account->save();

    //update facebook id for user. Write in pass field
    $update_fbid = db_update('users')->fields(array('pass' => $details['id']))->condition('uid', $account->id(), '=')->execute();
    

    return $account;
  }

  /*
  * Function check exists user
  */
  public function existsUser($fbid){

    //sql request for users
    $sql = "SELECT pass FROM users WHERE pass = :fbid";

    //query to db and set virble $fbid 
    $result = db_query($sql, array(
      ':fbid' => $fbid,
    ));

    $strings = $result->fetchAll();
    
    //return count of users 0 or 1 (true or false)
    return (bool) count($strings);
  }

  /*
  * Get user by fbid
  */
  public function selectUserByFBid($field, $fbid){
    
    //sql request for db
    $sql = "SELECT * FROM users WHERE pass = :fbid LIMIT 1";
    $result = db_query($sql, array(':fbid' => $fbid))->fetchAssoc();
    
    //get rows from request
    return $result[$field];
  }

  /*
  * Create Page 
  */
  public function LoLoginPage() {
    
    global $user;
    global $base_url;

    //parsing configuration file
    $config=$this->config('LoLogin.settings');

    //array with redirect parmas
    $par=array(
      'url' =>       $base_url,
      'app_id' =>    $config->get('linkedin_api_key'),
      'app_secret' => $config->get('linkedin_app_secret')
    );    

    //If facebook return code
    if(isset( $_GET['code'])) {
        
        //url to oauth
        $url = 'https://www.linkedin.com/uas/oauth2/accessToken';
        
        //hide curl outputs
        ob_start();

        //First request to get access token
        $curl_outp="";
        $curl_handle=curl_init();
        curl_setopt($curl_handle, CURLOPT_URL, $url);
        curl_setopt($curl_handle, CURLOPT_POST, true);
        curl_setopt($curl_handle, CURLOPT_POSTFIELDS, 'grant_type=authorization_code&code='.$_GET['code'].'&redirect_uri='.$base_url . '/llogin&client_id='.$par['app_id'].'&client_secret='.$par['app_secret']);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
        $query=curl_exec($curl_handle);
        curl_close($curl_handle);
        $decoded_qery_res=json_decode($query);

        //Request to get params
        $curl_handle=curl_init();
        curl_setopt($curl_handle, CURLOPT_URL, 'https://api.linkedin.com/v1/people/~?oauth2_access_token='.$decoded_qery_res->access_token);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
        $li_user_params = curl_exec($curl_handle);
        curl_close($curl_handle);

        //close outpusts
        ob_clean();

        //explode params
        $spl_par=explode(' ', $li_user_params);

        //get id
        $split_id=explode('=', $spl_par[16]);
        $split_id=explode('&', $split_id[1]);
        $split_id=$split_id[0];
        
        //make userdate array
        $user_params=array(
          'name' => $spl_par[5].$spl_par[7],
          'id' => $split_id
        );
        
        //check user exists
        if($this->existsUser($user_params['id'])){

            //if exists load user by id
            $svu=user_load($this->selectUserByFBid('uid', $user_params['id']));

        }else{

            //if not exists create new user
            $svu=$this->saveUser($user_params);
            
        }

        //Login new user 
        user_login_finalize($svu);

        $build=array('#markup' =>'Welcome to '.$base_url.','.$user_params['name'].'<meta http-equiv="refresh" content="0; url='.$base_url.'" />');

    }else{
      if(isset($_GET['redirect'])){
        

        //Request for code 
        $redirect_url='https://www.linkedin.com/uas/oauth2/authorization?response_type=code&client_id='.$par['app_id'].'&scope=r_fullprofile&state=T3ER5TVCs6Y45667I&redirect_uri='.$base_url.'/llogin';

        $build=array('#markup' => '<meta http-equiv="refresh" content="0; url='.$redirect_url.'" />');
      }else{
        if(isset($_GET['error'])){
          $build=array('#markup' => t('Error login via LinkedIn: '+$_GET['error']));
        }else{
          $build=array('#markup' => t(''));
        }
        
      }
      
    }

    return $build;
  }
 
} 