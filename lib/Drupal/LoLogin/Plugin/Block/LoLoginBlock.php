<?php


/**
 * @file
 * Contains \Drupal\LoLogin\Plugin\Block\LoLoginBlock.
 */



namespace Drupal\LoLogin\Plugin\Block;

use Drupal\block\Annotation\Block;
use Drupal\block\BlockBase;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Config;

/**
 * Provides a 'LoLogin' block.
 *
 * @Block(
 *   id = "LoLoginBlock",
 *   subject = @Translation("LinkedIn authentication"),
 *   admin_label = @Translation("LinkedIn authentication")
 * )
 */
class LoLoginBlock extends BlockBase {

  /**
   * Implements \Drupal\block\BlockBase::blockBuild().
   */
  public function build() {
    global $base_url;

    //Generate url to login
    return array(
        '#type' => 'markup',
        '#markup' => '<a href="'.$base_url.'/llogin?redirect=1">Login with LinkedIn</a>'
      );
  }

}
