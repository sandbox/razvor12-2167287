<?php
 
/**
 * @file
 * Contains \Drupal\Tologin\Form\LologinSettingsForm
 */
 
namespace Drupal\LoLogin\Form;
 
use Drupal\Core\Form\ConfigFormBase;
 
class LoLoginSettingsForm extends ConfigFormBase{
  
  public function getFormId()
  {
    return 'LoLogin_configure';
  }
  
  /**
   * Implements \Drupal\Core\Form\FormInterface::buildForm().
   */
  public function buildForm(array $form, array &$form_state)
  {
    $config = $this->configFactory->get('LoLogin.settings');

    $form['LoLogin'] = array(
      '#type' => 'fieldset',
      '#title' => t('Settings'),
    );
    
    $form['LoLogin']['linkedin_api_key'] = array(
      '#type' => 'textfield',
      '#title' => t('LinkedIn API key'),
      '#default_value' => $config->get('linkedin_api_key'),
    );
    
    
    $form['LoLogin']['linkedin_app_secret'] = array(
      '#type' => 'textfield',
      '#title' => t('LinkedIn client secret'),
      '#default_value' => $config->get('linkedin_app_secret'),
    );

    $form['LoLogin']['linkedin_app_token'] = array(
      '#type' => 'textfield',
      '#title' => t('LinkedIn app token'),
      '#default_value' => $config->get('linkedin_app_token'),
    );

    $form['LoLogin']['linkedin_app_token_secret'] = array(
      '#type' => 'textfield',
      '#title' => t('LinkedIn app token secret'),
      '#default_value' => $config->get('linkedin_app_token_secret'),
    );

    
    
    
    return parent::buildForm($form, $form_state);
  }
  
  /**
   * Implements \Drupal\Core\Form\FormInterface::submitForm().
   */
  public function submitForm(array &$form, array &$form_state)
  {
    $this
    ->configFactory
    ->get('LoLogin.settings')
    ->set('linkedin_api_key', $form_state['values']['linkedin_api_key'])
    ->set('linkedin_app_secret', $form_state['values']['linkedin_app_secret'])
    ->set('linkedin_app_token', $form_state['values']['linkedin_app_token'])
    ->set('linkedin_app_token_secret', $form_state['values']['linkedin_app_token_secret'])
    ->save();
    
    parent::submitForm($form, $form_state); 
  }
  
}